// JavaScript Document
/*********************/
// Location Search Filter
//<![CDATA[
    $(window).load(function() { // makes sure the whole site is loaded
        $('.sonar-wrapper').fadeOut(); // will first fade out the loading animation
        $('#loader').delay(50).fadeOut('slow'); // will fade out the white DIV that covers the website.
        $('body').delay(50).css({'overflow':'visible'});
    })
//]]>
//end ocation Search Filter
//Check to see if the window is top if not then display button
$(window).scroll(function(){
    if ($(this).scrollTop() > 100) {
        $('.scrollToTop').fadeIn();
    } else {
        $('.scrollToTop').fadeOut();
    }
    });	
    $('.scrollToTop').click(function(){
        $('html, body').animate({scrollTop : 0},700);
    return false;
});		
//endScrollToTop


$(document).ready(function() {    
    //on hover Menu 
    // $('.head_login .dropdown').hover(function() {
    //     $(this).find('.dropdown-menu').stop(true, true).delay(0).fadeIn(0);
    // },
    // function() {
    //     $(this).find('.dropdown-menu').stop(true, true).delay(0).fadeOut(0);
    // });	

    //Sticky Menu
    $('#nav_bg').stickit({scope: StickScope.Document, zIndex: 101}); 
    $('#nav_media').stickit({scope: StickScope.Document, zIndex: 101});    

    //Side Menu
    $("#sidebar").mCustomScrollbar({
        theme: "minimal"
    });
    $('#dismiss, .overlay').on('click', function () {
        $("body").removeClass("nav_open");
        $('#sidebar').removeClass('active');
        $('.overlay').removeClass('active');        
    });
    $('#sidebarCollapse').on('click', function () {
        $("body").addClass("nav_open");
        $('#sidebar').addClass('active');
        $('.overlay').addClass('active');
        $('.collapse.in').toggleClass('in');
        $('a[aria-expanded=true]').attr('aria-expanded', 'false');
    });
    // Products add
    $(".btn-minus").on("click",function(){
        var now = $(".addQty_btn > div > input").val();
        if ($.isNumeric(now)){
            if (parseInt(now) -1 > 0){ now--;}
            $(".addQty_btn > div > input").val(now);
        }else{
            $(".addQty_btn > div > input").val("1");
        }
    })            
    $(".btn-plus").on("click",function(){
        var now = $(".addQty_btn > div > input").val();
        if ($.isNumeric(now)){
            $(".addQty_btn > div > input").val(parseInt(now)+1);
        }else{
            $(".addQty_btn > div > input").val("1");
        }
    }) 
    //

    $(".modal").on("show", function () {
        $("body").addClass("modal-open");
    }).on("hidden", function () {
        $("body").removeClass("modal-open")
    });

})









	
