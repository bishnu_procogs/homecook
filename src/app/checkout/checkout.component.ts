import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import { Router } from "@angular/router";

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  model: any = {};
  loginForm: boolean;
  otpForm: boolean;
  signupForm: boolean;
  loginSuccess: boolean;
  deliveryOpen: boolean;
  paymentOpen: boolean;

  categories: any = [];
  full_address: string;
  message: string;
  msg_class: string;
  sub_total: number;
  tot_item: number = 0;
  quantity: any = {};
  cartItems: any[] = JSON.parse(localStorage.getItem('cart_products'));

  constructor(public rest: RestService, private router: Router) { }

  ngOnInit() {
    this.loginForm = false;
    this.otpForm = false;
    this.signupForm = false;
    this.loginSuccess = false;
    this.deliveryOpen = false;
    this.paymentOpen = false;

    // Get Current Location
    let restoredLocation = JSON.parse(localStorage.getItem('current_location'));
    if (restoredLocation == null || restoredLocation == '') {
      this.router.navigate(['/location']);
    }
    else {
      this.full_address = restoredLocation.full_address;
    }

    this.getCartItems();
  }

  // Get Cart Items
  getCartItems() {
    let restoredProducts = JSON.parse(localStorage.getItem('cart_products'));

    let subTotal = 0;
    for (let i = 0; i < restoredProducts.length; i++) {
      subTotal = subTotal + parseFloat(restoredProducts[i].tot_price);
    }
    this.sub_total = subTotal;
    this.tot_item = restoredProducts.length;

    if (restoredProducts.length == 0) {
      this.router.navigate(['/order']);
    }
    else {
      return restoredProducts;
    }
  }

  // Decrease Quantity
  decreaseQuantity(event, index) {
    let target = event.target || event.srcElement || event.currentTarget;
    let curQty = parseFloat(target.getAttribute('data-val'));
    let qty = curQty - 1;

    if (qty > 0) {
      let restoredProducts = JSON.parse(localStorage.getItem('cart_products'));
      restoredProducts[index].quantity = qty;
      restoredProducts[index].tot_price = parseFloat(restoredProducts[index].price) * qty;
      localStorage.setItem("cart_products", JSON.stringify(restoredProducts));

      this.cartItems = this.getCartItems();
    }
    else {
      if (confirm('Item will be removed from cart. Do you want to continue?')) {
        let restoredProducts = JSON.parse(localStorage.getItem('cart_products'));
        restoredProducts.splice(index, 1);
        localStorage.setItem("cart_products", JSON.stringify(restoredProducts));
        this.cartItems = this.getCartItems();
      }
    }
  }

  // Increase Quantity
  increaseQuantity(event, index) {
    let target = event.target || event.srcElement || event.currentTarget;
    let curQty = parseFloat(target.getAttribute('data-val'));
    let qty = curQty + 1;

    let restoredProducts = JSON.parse(localStorage.getItem('cart_products'));
    restoredProducts[index].quantity = qty;
    restoredProducts[index].tot_price = parseFloat(restoredProducts[index].price) * qty;
    localStorage.setItem("cart_products", JSON.stringify(restoredProducts));

    this.cartItems = this.getCartItems();
  }

  // Login
  loginAction(form) {
    console.log(form.value);
    this.model.mobile_verify = form.value.mobile;
    this.loginForm = false;
    this.otpForm = true;

    let restoredLocation = JSON.parse(localStorage.getItem('current_location'));
    this.full_address = restoredLocation.full_address;
  }

  // Login
  verifyOtpAction(form) {
    console.log(form.value);
    
    this.loginSuccess = true;
    this.deliveryOpen = true;

    // click on step 2 element
    document.getElementById('step-2-tab').click();

    let restoredLocation = JSON.parse(localStorage.getItem('current_location'));
    this.full_address = restoredLocation.full_address;
  }

  // Register
  registerAction(form) {
    
    console.log(form.value);
    this.model.mobile_verify = form.value.mobile;
    this.model.mobile = form.value.mobile;
    this.signupForm = false;
    this.otpForm = true;

    /*this.rest.callPostApi('customer-register',{fullname:form.value.name,email:form.value.email,mobile:form.value.mobile}).subscribe((response) => {
      if(response.status=="true") {
        form.resetForm();
        this.message = response.msg;
        this.msg_class = 'alert-success';
        
        //this.loginSuccess = true;
        //this.deliveryOpen = true;
      }
      else {
        this.message = response.msg;
        this.msg_class = 'alert-danger';
        this.loginSuccess = false;
        this.deliveryOpen = false;
      }
    });*/

    let restoredLocation = JSON.parse(localStorage.getItem('current_location'));
    this.full_address = restoredLocation.full_address;
  }

  // save delivery address
  saveDelivery() {
    this.deliveryOpen = false;
    this.paymentOpen = true;

    // click on step 3 element
    document.getElementById('step-3-tab').click();

    let restoredProducts = JSON.parse(localStorage.getItem('cart_products'));

    let subTotal = 0;
    for (let i = 0; i < restoredProducts.length; i++) {
      subTotal = subTotal + parseFloat(restoredProducts[i].tot_price);
    }
    this.sub_total = subTotal;
  }

}
