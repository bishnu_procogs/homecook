import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.css']
})
export class LocationComponent implements OnInit {

  model: any = {};

  public options = {type : 'address', componentRestrictions: { country: 'IN' }};

  constructor(private router: Router) { }

  ngOnInit() {
    let restoredLocation = JSON.parse(localStorage.getItem('current_location'));
    if(restoredLocation==null || restoredLocation=='') {
    }
    else {
      let restoredLocation = JSON.parse(localStorage.getItem('current_location'));
      this.model.full_address = restoredLocation.full_address;
      this.model.cur_lat = restoredLocation.latitude;
      this.model.cur_lon = restoredLocation.longitude;
    }
  }

  saveLocation(form) {
    let locationObj = { full_address: form.full_address, latitude: form.cur_lat, longitude:form.cur_lon };
    localStorage.setItem('current_location', JSON.stringify(locationObj));
    this.router.navigate(['/order']);
  }

  // Geo Location
  // Geo Location
  getAddress(event: any) {
    console.log(event);
    this.model.full_address = event.name+', '+event.formatted_address;
  }

  getFormattedAddress(event: any) {
    console.log(event);
    this.model.cur_lat = event.lat;
    this.model.cur_lon = event.lng;
  } 

}
