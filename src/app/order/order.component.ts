import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  categories:any = [];
  deliveryItem:boolean;
  pickupItem:boolean;
  full_address:string;
  deliveryItems:any = [];
  pickupItems:any = [];
  sub_total:number;
  tot_item:number = 0;
  quantity:any = {};
  cartItems:any[] = JSON.parse(localStorage.getItem('cart_products'));

  constructor(public rest:RestService,private router: Router) { }

  ngOnInit() {
    this.deliveryItem=true;
    this.pickupItem=false;
    // Get Current Location
    let restoredLocation = JSON.parse(localStorage.getItem('current_location'));
    if(restoredLocation==null || restoredLocation=='') {
      this.router.navigate(['/location']);
    }
    else {
      this.full_address = restoredLocation.full_address;
    }

    this.getCategories();
    this.getItems();
    this.getCartItems();
  }

  hideModal() {
    document.getElementById('cart-modal-close').click();
  }


  // Get Food Categories
  getCategories() {
    this.categories = [];
    this.rest.callPostApi('get-categories',{}).subscribe((response) => {
      if(response.status=="true") {
        this.categories = response.data;
      }
    });
  }

  // Get Food Items
  getItems() {
    // Get Current User Location
    let restoredLocation = JSON.parse(localStorage.getItem('current_location'));
    let cur_latitude = restoredLocation.latitude;
    let cur_longitude = restoredLocation.longitude;

    this.deliveryItems = [];
    this.rest.callPostApi('get-delivarable-items',{cur_lat:cur_latitude,cur_lon:cur_longitude}).subscribe((response) => {
      if(response.status=="true") {
        this.deliveryItems = response.data;
      }
    });

    this.pickupItems = [];
    this.rest.callPostApi('get-pickup-items',{cur_lat:cur_latitude,cur_lon:cur_longitude}).subscribe((response) => {
      if(response.status=="true") {
        this.pickupItems = response.data;
      }
    });
  }

  // Get Cart Items
  getCartItems() {
    let restoredProducts = JSON.parse(localStorage.getItem('cart_products'));

    let subTotal = 0;
    for(let i=0;i<restoredProducts.length;i++) {
        subTotal = subTotal+parseFloat(restoredProducts[i].tot_price);
    }
    this.sub_total = subTotal;
    this.tot_item = restoredProducts.length;

    return restoredProducts;
  }

  // Add to Cart
  addToCart(event) {
    let target = event.target || event.srcElement || event.currentTarget;
    let idAttr = target.attributes.id;
    let productId = idAttr.nodeValue;
    let productName = target.getAttribute('data-name');
    let productPrice = target.getAttribute('data-price');;


    var arrPro = [];
    var restoredProducts = JSON.parse(localStorage.getItem('cart_products'));


    if(restoredProducts==null) {
        let proObj = { id: productId, name: productName, quantity:1, price: productPrice, tot_price: productPrice };
      
        let subTotal = parseFloat(productPrice);

        arrPro.push(proObj);

        localStorage.setItem('cart_products', JSON.stringify(arrPro));

        this.tot_item = 1;
        this.cartItems = arrPro;
        this.sub_total = subTotal;
    }
    else {
        let restoredProducts = JSON.parse(localStorage.getItem('cart_products'));

        let subTotal = 0;

        for(let i=0;i<restoredProducts.length;i++) {

            subTotal = subTotal+parseFloat(restoredProducts[i].tot_price);

            let proObj = { id: restoredProducts[i].id, name: restoredProducts[i].name, quantity:restoredProducts[i].quantity, price: restoredProducts[i].price, tot_price: restoredProducts[i].tot_price };
            arrPro.push(proObj);

            localStorage.setItem('cart_products', JSON.stringify(arrPro));
            
        }

        let proObj = { id: productId, name: productName, quantity:1, price: productPrice, tot_price: productPrice };
        arrPro.push(proObj);
        localStorage.setItem('cart_products', JSON.stringify(arrPro));

        this.tot_item = restoredProducts.length+1;
        this.sub_total = subTotal+parseFloat(productPrice);
        this.cartItems = arrPro;
    }
  }

  // Remove From Cart
  removeFromCart(event) {
    let target = event.target || event.srcElement || event.currentTarget;
    let idAttr = target.attributes.id;
    let productId = idAttr.nodeValue;

    let restoredProducts = JSON.parse(localStorage.getItem('cart_products'));
    restoredProducts = restoredProducts.filter(data => data.id !== productId);
    localStorage.setItem('cart_products', JSON.stringify(restoredProducts));

    let subTotal = 0;
    for(let i=0;i<restoredProducts.length;i++) {
      subTotal = subTotal+parseFloat(restoredProducts[i].price);
    }

    this.tot_item = restoredProducts.length-1;
    this.sub_total = subTotal;
    this.cartItems = [];
    this.cartItems = restoredProducts;
  }

  // Decrease Quantity
  decreaseQuantity(event,index) {
	  let target = event.target || event.srcElement || event.currentTarget;
    let curQty = parseFloat(target.getAttribute('data-val'));
    let qty = curQty-1;
    
    if(qty>0) {
      let restoredProducts = JSON.parse(localStorage.getItem('cart_products'));
      restoredProducts[index].quantity = qty;
      restoredProducts[index].tot_price = parseFloat(restoredProducts[index].price) * qty;
      localStorage.setItem("cart_products",JSON.stringify(restoredProducts));
      
      this.cartItems = this.getCartItems();
    }
    else {
      if(confirm('Item will be removed from cart. Do you want to continue?')) {
        let restoredProducts = JSON.parse(localStorage.getItem('cart_products'));
        restoredProducts.splice(index, 1);
        localStorage.setItem("cart_products",JSON.stringify(restoredProducts));
        this.cartItems = this.getCartItems();
      }
    }
  }
  
  // Increase Quantity
  increaseQuantity(event,index) {
	  let target = event.target || event.srcElement || event.currentTarget;
    let curQty = parseFloat(target.getAttribute('data-val'));
    let qty = curQty+1;
	  
	  let restoredProducts = JSON.parse(localStorage.getItem('cart_products'));
    restoredProducts[index].quantity = qty;
    restoredProducts[index].tot_price = parseFloat(restoredProducts[index].price) * qty;
	  localStorage.setItem("cart_products",JSON.stringify(restoredProducts));
	  
	  this.cartItems = this.getCartItems();
  }

}
