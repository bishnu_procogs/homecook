import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HeaderComponent } from './common/header/header.component';
import { FooterComponent } from './common/footer/footer.component';
import { OrderComponent } from './order/order.component';
import { LocationComponent } from './location/location.component';
import { CheckoutComponent } from './checkout/checkout.component';

const routes: Routes = [
  { path: '', redirectTo: '/location', pathMatch: 'full' },
  { path:'order', component:OrderComponent },
  { path:'location', component:LocationComponent },
  { path:'checkout', component:CheckoutComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [OrderComponent,HeaderComponent,FooterComponent,LocationComponent,CheckoutComponent]
